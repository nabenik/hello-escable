package com.nabenik.hello;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/hello")
public class HolaEndpoint {
    
    @GET
    public String doHello(@QueryParam("name")
            String name){
        return "Hola " + name;
    }
    
}
